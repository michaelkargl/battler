# Aaya Battler

During my weekly exchanges with one of my pen-pals... and the fact that english is my second language, I might have called my friend a "good drawer" when I meant "skillfull painter". She replied with a drawing of her wielding the most powerful weapon "The Blush-Gun". A gun that is showering you with shameless compliments until you break.

I thought it to be funny to actually put that scene into motion. The intention was to make this a timeboxed thing so it is far from perfect. Learned a lot about HTML, CSS and once again RxJS :)

## Progress

Thought it might be fun to record the steps taken throughout the development cycles.
Thanks again to @NickeManarin  for developing https://github.com/NickeManarin/ScreenToGif/ <3

![](./aaya-battler-makingof.gif)

## How to run

Run it by executing `npm run develop`

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):


## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Gatsby CLI
1. Generate and preview the website with hot-reloading: `gatsby develop`
1. Add content

