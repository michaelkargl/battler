module.exports = {
  pathPrefix: '/battler',
  siteMetadata: {
    title: 'Gatsby Default Starter',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-typescript',
    {
      resolve: 'gatsby-plugin-sass',
      options: {
        /** @summary This plugin resolves url() paths relative to the entry SCSS/Sass 
        *           file not – as might be expected – the location relative to the declaration. 
        *           Under the hood, it makes use of sass-loader and this is documented in the readme.
        *           Using resolve-url-loader provides a workaround, if you want to use relative url 
        *           just install the plugin and then add it to your sass plugin options configuration.
        * @see https://www.gatsbyjs.org/packages/gatsby-plugin-sass/#relative-paths--url 
        * @see https://github.com/bholloway/resolve-url-loader/blob/master/packages/resolve-url-loader/README.md
        */
        useResolveUrlLoader: true
      }

    }
  ],
}
