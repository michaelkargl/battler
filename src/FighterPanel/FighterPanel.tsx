import React from "react";
import CombatantPanel from "../CombatantPanel/CombatantPanel";
import Combatant from "../types/Combatant";


interface FighterPanelProps {
    combatant: Combatant
}

export default class FighterPanel extends React.Component<FighterPanelProps> {
    
    render() {
        return (<div className="fighter-panel">
            <CombatantPanel combatant={this.props.combatant}></CombatantPanel>
        </div>);
    }
}