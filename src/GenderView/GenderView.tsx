import React from "react";
import { Gender } from "../types/Gender";

import './GenderView.scss';

interface GenderViewProps {
    gender: Gender
}

export default class GenderView extends React.Component<GenderViewProps> {

    render() {
        return (<span className="gender-view">
            {this.props.gender === Gender.Female ? '♀️' : '♂️'}
        </span>);
    }
}