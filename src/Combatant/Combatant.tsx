import React from "react";

import './Combatant.scss';

interface CombatantProps {
    image: string
}

export default class Combatant extends React.Component<CombatantProps> {
    render() {
        return (<div className="combatant centered-content">
            <img src={this.props.image}></img>
        </div>);
    }
}