import React from "react";
import FighterPanel from "../FighterPanel/FighterPanel";
import BattleGround from "../BattleGround/BattleGround";
import ControlPanel from "../ControlPanel/ControlPanel";
import Scene from "../types/Scene";
import { Gender } from "../types/Gender";
import { CombatantTypes } from "../types/CombatantType";
import { Guid } from 'guid-typescript';
import AttackPanel from "../AttackPanel/AttackPanel";
import { Attack } from "../types/Attack";
import { Combatant } from "../types/Combatant";
import HitpointAttack from "../types/attacks/HitPointAttack";
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import SwapAttack from "../types/attacks/SwapAttack";

import combatantImagePlayer from '../assets/images/combatant-player.png';
import combatantImageOponent from '../assets/images/combatant-opponent.png';
import combatantImageOpponentBlush from '../assets/images/combatant-opponent_blush.png';
import combatantImageOpponentApathic from '../assets/images/combatant-opponent_apathic.png';
import combatantImageOpponentOolala from '../assets/images/combatant-opponent_oolala.png';
import combatantImageOpponentOuch from '../assets/images/combatant-opponent_ouch.png';
import combatantImageOpponentDead from '../assets/images/combatant-opponent_dead.png';
import './BattleScene.scss';
import CombiAttack from "../types/attacks/CombiAttack";
import AttackService from "../AttackService";

interface BattleSceneState {
    scene: Scene
}

export default class BattleScene extends React.Component<any, BattleSceneState> {
    protected readonly attackService: AttackService;

    private scene: Scene = {
        combatants: {
            player: new Combatant({
                type: CombatantTypes.Player,
                name: "Aaya",
                gender: Gender.Female,
                healthPoints: 100,
                totalHealthPoints: 100,
                level: 1,
                image: combatantImagePlayer,
                attacks: [
                    new CombiAttack(
                        "Tackle",
                        new SwapAttack("Blush-gun", combatantImageOpponentOuch),
                        ...this.range(20).map(i => new HitpointAttack(`Tackle ${i}`, 2))
                    ),
                    new CombiAttack(
                        "Hug",
                        new SwapAttack("Hug", combatantImageOpponentOolala),
                        ...this.range(20).map(i => new HitpointAttack(`Heal ${i}`, -2))
                    ),
                    new SwapAttack("Shrug", combatantImageOpponentApathic),
                    new CombiAttack(
                        "Blush-gun",
                        ...this.range(51).map(i => new HitpointAttack(`Healgun ${i}`, -2)),
                        new SwapAttack("Blush-gun", combatantImageOpponentBlush, c => c.healthPoints > 100)
                    )
                ]
            }),
            opponent: new Combatant({
                type: CombatantTypes.Challenger,
                name: "Michisaurus",
                gender: Gender.Male,
                healthPoints: 100,
                totalHealthPoints: 100,
                level: 999,
                image: combatantImageOponent,
                attacks: []
            })
        }
    };

    constructor(props: any) {
        super(props);
        this.state = {
            scene: this.scene
        };

        this.attackService = new AttackService();
    }


    private async handleAttack(attack: Attack, source: Combatant, target: Combatant): Promise<void> {
        ('Combatant %s attacks %s with %s', source.name, target.name, attack.name);

        const target$ = this.attackService.attack(target, attack);

        const attackCompleted$ = new Subject<void>();
        target$.pipe(
            takeUntil(attackCompleted$)
        ).subscribe(updatedTarget => {
            this.updateCombatant(updatedTarget);
        },
            error => error,
            () => {
                console.debug('Attack ended, closing subscription.');
                attackCompleted$.next();
            }
        );
    }

    private updateCombatant(combatant: Combatant) {

        const combatants = combatant.type === CombatantTypes.Player
            ? { ...this.scene.combatants, player: combatant }
            : { ...this.scene.combatants, opponent: combatant };

        this.setState({
            scene: {
                ...this.state.scene,
                combatants
            }
        });
    }

    private range(n: number): number[] {
        return [...Array(n).keys()];
    }


    render() {
        //TODO: This code will be executed on every render, rethink this approach
        const [opponent, player] = [
            this.state.scene.combatants.opponent,
            this.state.scene.combatants.player
        ];

        if (!opponent || !player)
            throw new Error('No challenger or player has been defined. Please validate your scene definition');

        return (<div className="scene">
            <div className="fighter-top">
                <FighterPanel combatant={opponent}></FighterPanel>
            </div>

            <BattleGround></BattleGround>

            <div className="fighter-bottom">
                <FighterPanel combatant={player}></FighterPanel>
            </div>

            <div className="control-panel">
                <ControlPanel>
                    <AttackPanel
                        attacks={player.attacks}
                        onAttackClick={(attack) => this.handleAttack(attack, player, opponent)}>
                    </AttackPanel>
                </ControlPanel>
            </div>
        </div>);
    }
}