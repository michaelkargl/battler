import React from "react";

import backgroundImage from '../assets/images/progressbar-border.png';
import fillingImage from '../assets/images/progressbar-filling.png';


interface ProgressBarProps {
    progress: number;
}


import './ProgressBar.scss';

export default class ProgressBar extends React.Component<ProgressBarProps> {

    render() {
        return (<div className="progress-bar">
            <div className="progress-bar-filling" style={{
                backgroundImage: `url(${fillingImage})`,
                backgroundRepeat: 'no-repeat',
                width: `${this.props.progress > 100 ? 100 : this.props.progress}%`
            }}>
            </div>
            <div className="progress-bar-border" style={{
                backgroundImage: `url(${backgroundImage})`,
                backgroundRepeat: 'no-repeat',
            }}>
            </div>
            
        </div>);
    }
}