import React from "react";
import Combatant from "../types/Combatant";

import './CombatantInfos.scss';
import ProgressBar from "../ProgressBar/ProgressBar";
import GenderView from "../GenderView/GenderView";

export interface CombatantInfosProps {
    backgroundImage: string;
    combatant: Combatant
}


export default class CombatantInfos extends React.Component<CombatantInfosProps> {
    render() {
        const { combatant } = this.props;

        return (<div className="combatant-infos combatant-infos--background " style={{
            backgroundImage: `url(${this.props.backgroundImage})`
        }}>
            <div className="combatant-infos__content--spacing">
                <div className="combetant-info-progressbar--sizing">
                    <span>{combatant.name}</span>

                    <div className="combetant-info-stats--position">
                        <div className="combetant-info-progressbar_hp">
                            <GenderView gender={combatant.gender}></GenderView>
                        Lv {combatant.level}
                        </div>

                        <ProgressBar progress={combatant.healthPoints}></ProgressBar>

                        <div className="combetant-info-progressbar_hp">
                            {combatant.healthPoints} / {combatant.totalHealthPoints}
                        </div>
                    </div>
                </div>
            </div>
        </div>);
    }
}