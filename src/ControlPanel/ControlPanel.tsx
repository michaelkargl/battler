import React from "react";

import backgroundImage from '../assets/images/control-panel-border-bg.png';

import './ControlPanel.scss';
import AttackPanel from "../AttackPanel/AttackPanel";
import Scene from "../types/Scene";
import { CombatantTypes } from "../types/CombatantType";
import Combatant from "../types/Combatant";

interface ControlPanelProps {
    scene: Scene,
    player: Combatant
}


export default class ControlPanel extends React.Component<ControlPanelProps> {
    render() {
        return (<div className="control-panel" style={{
            backgroundImage: `url(${backgroundImage})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: '100% 100%',
            height: '100%',
            width: '100%'
        }}>
            {this.props.children}
        </div>);
    }
}