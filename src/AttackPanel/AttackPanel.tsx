import React from "react";
import { Attack } from "../types/Attack";

import './AttackPanel.scss';

interface AttackPanelProps {
    attacks: Attack[];
    onAttackClick?: (attack: Attack) => void;
}


export default class AttackPanel extends React.Component<AttackPanelProps> {

    private attackClicked(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>, attack: Attack) {
        const { onAttackClick } = this.props;
        if (onAttackClick) {
            onAttackClick(attack);    
        }
    }

    render() {
        const { attacks } = this.props;

        return (<div className="attack-panel attack-panel--layout attack-panel--spacing">
            {
                attacks.slice(0, 4).map(attack =>
                    <a className="attack-link--coloring text-center" 
                        href={`#${attack.id}`}
                        onClick={(event) => this.attackClicked(event, attack)}>
                        {attack.name}
                    </a>
                )
            }
        </div>);
    }
}