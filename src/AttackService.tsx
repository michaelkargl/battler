import { Combatant } from "./types/Combatant";
import { Attack } from "./types/Attack";
import { Observable, of, interval, zip, merge } from "rxjs";
import { switchMap, map, concat, concatAll } from "rxjs/operators";

export default class AttackService {

    attack(combatant: Combatant, ...attacks: Attack[]): Observable<Combatant> {
        // console.log('Attacking %s instantly with %i attacks: %o', combatant.name, attacks.length, attacks);

        const combatantHistoryStackIterator = this.applyAttacks(of(combatant), ...attacks);
        const combatantHistory = [...combatantHistoryStackIterator].reverse();

        const combatantHistory$ = merge(...combatantHistory);
        return combatantHistory$;
    }


    attackOverTime(combatant: Combatant, durationMs: number, ...attacks: Attack[]): Observable<Combatant> {
        const combatantHistory$ = this.attack(combatant, ...attacks);

        const historyOverTime$ = zip(
            combatantHistory$,
            interval(durationMs)
        ).pipe(
            map(([combatant, i]) => combatant)
        );

        return historyOverTime$;
    }

    /**
     * @param combatant$
     * @param attacks 
     * @returns Iterator over combatant attack history stack
     */
    private *applyAttacks(combatant$: Observable<Combatant>, ...attacks: Attack[]): IterableIterator<Observable<Combatant>> {
        const [currentAttack, ...nextAttacks] = attacks;

        if (!currentAttack)
            return;        
        
        const attackedCombatant$ = combatant$.pipe(
            switchMap(c => currentAttack.execute(c))
        );
        
        yield* this.applyAttacks(attackedCombatant$, ...nextAttacks);
        
        yield attackedCombatant$;
    }
}