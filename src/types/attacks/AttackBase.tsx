import { Attack } from "../Attack";
import { Guid } from 'guid-typescript';
import { Observable, from, interval, zip } from 'rxjs';
import { Combatant } from "../Combatant";


export abstract class AttackBase implements Attack {
    readonly tickDurationMs: number = 1000;
    readonly id: Guid;
    readonly name: string;
    readonly hitPoints: number;

    constructor(
        name: string,
        hitPoints: number
    ) {
        this.id = Guid.create();
        this.name = name;
        this.hitPoints = hitPoints;
    }
    
    abstract execute(target: Combatant): Observable<Combatant>;
}
