import { AttackBase } from "./AttackBase";
import { Combatant } from "../Combatant";
import { Attack } from "../Attack";
import { Observable } from "rxjs";
import AttackService from "../../AttackService";


export default class CombiAttack extends AttackBase {
    private readonly attackService: AttackService; 
    private readonly attacks: Attack[];

    constructor(name: string, ...attacks: Attack[]) {
        super(name, 0);

        this.attacks = attacks;
        this.attackService = new AttackService();
    }

    execute(combatant: Combatant): Observable<Combatant> {
        return this.attackService.attackOverTime(combatant, 50, ...this.attacks);
    }
}