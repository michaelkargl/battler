import { AttackBase } from "./AttackBase";
import { Combatant } from "../Combatant";
import { Observable, of } from 'rxjs';


export default class HitpointAttack extends AttackBase {
    
    constructor(name: string, hitPoints: number) {
        super(name, hitPoints);
        
    }

    execute(combatant: Combatant): Observable<Combatant> {
        return of(new Combatant({
            ...combatant,
            healthPoints: combatant.healthPoints - this.hitPoints
        }));
    }
}