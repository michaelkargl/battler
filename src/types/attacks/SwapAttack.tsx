import { AttackBase } from "./AttackBase"
import { Combatant } from "../Combatant";
import { Observable, of } from 'rxjs';


export default class SwapAttack extends AttackBase {
    private blushImage: string;
    private readonly attackCondition?: (combatant: Combatant) => boolean;

    constructor(
        name: string,
        blushImage: string,
        condition?: (combatant: Combatant) => boolean
    ) {
        super(name, 0);

        this.blushImage = blushImage;
        this.attackCondition = condition;
    }

    execute(combatant: Combatant): Observable<Combatant> {
        
        if ((this.attackCondition && this.attackCondition(combatant)) ?? true) {
            if (combatant.image !== this.blushImage) {
                combatant.image = this.blushImage;
            }
        } else {
            console.debug('%s attack condition not met. Combatant', this.name, combatant);
        }

        return of(combatant);
    }
}