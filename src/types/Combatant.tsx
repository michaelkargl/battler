import { Gender } from "./Gender";
import { CombatantTypes } from "./CombatantType";
import { Attack } from "./Attack";

export class Combatant {
    type: CombatantTypes;
    name: string;
    gender: Gender;
    level: number;
    totalHealthPoints: number;
    healthPoints: number;
    image: string;
    attacks: Attack[];


    constructor({
        type,
        name,
        gender,
        level,
        healthPoints,
        totalHealthPoints,
        image,
        attacks
    }: {
        type: CombatantTypes,
        name: string,
        gender: Gender,
        level: number,
        healthPoints: number,
        totalHealthPoints: number,
        image: string,
        attacks: Attack[]
    }) {
        this.type = type;
        this.name = name;
        this.gender = gender;
        this.level = level;
        this.healthPoints = healthPoints;
        this.totalHealthPoints = totalHealthPoints;
        this.image = image;
        this.attacks = attacks;
    }

    applyHitPoints(hitPoints: number) {
        this.healthPoints = this.healthPoints - hitPoints;
    }
}
