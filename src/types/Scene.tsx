import { Combatant } from '../types/Combatant'

export default interface Scene {
    combatants: {
        player: Combatant,
        opponent: Combatant
    }
}