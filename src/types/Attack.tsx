import { Guid } from 'guid-typescript';
import { Combatant } from '../types/Combatant';
import { Observable } from 'rxjs';


export interface Attack {
    id: Guid,
    name: string,
    hitPoints: number

    execute(target: Combatant, condition?: (combatant: Combatant) => boolean): Observable<Combatant>;
}