import React from "react";
import Combatant from "../Combatant/Combatant";
import CombatantInfos from "../CombatantInfos/CombatantInfos";

import backgroundImage from '../CombatantInfos/combatant-info-bg.png';
import './CombatantPanel.scss';
import CombatantType from "../types/Combatant";
import { CombatantTypes } from "../types/CombatantType";


interface CombatantPanelProps {
    combatant: CombatantType
}

export default class CombatantPanel extends React.Component<CombatantPanelProps> {

    render() {
        const { combatant } = this.props;
        const direction = combatant.type === CombatantTypes.Player ? 'left' : 'right';

        return (<div className={`combatant-panel combatant-area--${direction}`}>
            <div className="info-area">
                <CombatantInfos combatant={this.props.combatant}
                                backgroundImage={backgroundImage}></CombatantInfos>
            </div>
            <div className="combatant-area">
                <Combatant image={this.props.combatant.image}></Combatant>
            </div>
        </div>);
    }
}